module gyster

go 1.13

replace git.cto.ai/sdk-go => /go/src/gyster/pkg/sdk-go

require git.cto.ai/sdk-go v0.0.0
