package github

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Gist struct {
	Url         string              `json:"url`
	Id          string              `json:"id"`
	Description string              `json:"description"`
	Files       map[string]GistFile `json:"files"`
	Public      bool                `json:"public"`
}

type GistFile struct {
	Content   string `json:"content"`
	Filename  string `json:"filename"`
	Language  string `json:"language"`
	Size      int    `json:"size"`
	RawUrl    string `json:"raw_url"`
	Type      string `json:"type"`
	Truncated bool   `json:"truncated"`
}

type GistDraft struct {
	Description string
	Filename    string
	Content     string
	Public      bool
}

type GistRequest struct {
	Description string                     `json:"description"`
	Files       map[string]GistRequestFile `json:"files"`
	Public      bool                       `json:"public"`
}

type GistRequestFile struct {
	Content string `json:"content"`
}

func (g GistDraft) NotComplete() bool {
	return g.Description == "" || g.Filename == "" || g.Content == ""
}

func (g Gist) Has_files() bool {
	for _, v := range g.Files {
		if v.Content == "" {
			return false
		}

		break
	}

	return true
}

func (g *Gist) Get_files(url, token string) error {
	r := &github_request{
		name:     "get_files",
		method:   "GET",
		httppath: fmt.Sprintf("/gists/%s", g.Id),
		payload:  nil,
		response: nil,
	}

	err := r.request_github(url, token)
	if err != nil {
		return fmt.Errorf("Error in executing Get_files request: %s", err)
	}

	var gist Gist

	err = json.Unmarshal(r.response, &gist)
	if err != nil {
		return fmt.Errorf("Error in json unmarshalling response body: %s\n", err)
	}

	*g = gist

	return nil
}

func (g GistFile) Content_contains(ptrn string) bool {
	return strings.Contains(g.Content, ptrn)
}

func (g Gist) Get_file_names() []string {
	var files []string

	for k, _ := range g.Files {
		files = append(files, k)
	}

	return files
}

func NewGistRequest(g GistDraft) GistRequest {
	return GistRequest{
		Description: g.Description,
		Public:      g.Public,
		Files: map[string]GistRequestFile{
			g.Filename: GistRequestFile{
				Content: g.Content,
			},
		},
	}
}

func (g GistDraft) Create(url, token string) error {
	new_gist_request := NewGistRequest(g)

	bytes, err := json.Marshal(new_gist_request)
	if err != nil {
		return fmt.Errorf("Error in serializing gist payload to bytes: %s\n", err)
	}

	r := &github_request{
		name:     "create_gist",
		method:   "POST",
		httppath: fmt.Sprintf("/gists"),
		payload:  bytes,
		response: nil,
	}

	err = r.request_github(url, token)
	if err != nil {
		return fmt.Errorf("Error in executing create request: %s", err)
	}

	var gist Gist

	err = json.Unmarshal(r.response, &gist)
	if err != nil {
		return fmt.Errorf("Error in json unmarshalling response body: %s\n", err)
	}

	return nil
}
