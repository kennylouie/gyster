package github

import (
	"fmt"
	"gyster/internal/libgyster/request"
)

type github_request struct {
	name     string
	method   string
	httppath string
	payload  []byte
	response []byte
}

func (g *github_request) request_github(url, token string) error {
	op := &request.Operation{
		Name:       g.name,
		HttpMethod: g.method,
		URL:        fmt.Sprintf("%s%s", url, g.httppath),
		Payload:    g.payload,
		Headers:    map[string]string{"Authorization": fmt.Sprintf("token %s", token)},
	}

	r := request.New(op)

	err := r.Exec()
	if err != nil {
		return fmt.Errorf("Failed to execute github request: %s", err)
	}

	g.response = r.Data

	return nil
}
