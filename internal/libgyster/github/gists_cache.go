package github

import (
	"encoding/json"
	"fmt"
	"strings"
)

type GistsCache struct {
	Choices []string
	Gists   map[string]*Gist
}

func (g *GistsCache) Get_gists(user_name, url, token string) error {
	r := &github_request{
		name:     "get_gists",
		method:   "GET",
		httppath: fmt.Sprintf("/users/%s/gists", user_name),
		payload:  nil,
		response: nil,
	}

	err := r.request_github(url, token)
	if err != nil {
		return fmt.Errorf("Error in executing Get_gists request: %s", err)
	}

	var gists []Gist

	err = json.Unmarshal(r.response, &gists)
	if err != nil {
		return fmt.Errorf("Error in json unmarshalling response body: %s\n", err)
	}

	var choices []string

	var gists_cache_map map[string]*Gist
	gists_cache_map = make(map[string]*Gist, len(gists)-1)

	for _, gst := range gists {
		choices = append(choices, gst.Description)

		gists_cache_map[gst.Description] = &Gist{}

		*(gists_cache_map[gst.Description]) = gst
	}

	*g = GistsCache{
		Choices: choices,
		Gists:   gists_cache_map,
	}

	return nil
}

func (g *GistsCache) Get_gists_files(url, token string) error {
	for k, v := range g.Gists {
		if !v.Has_files() {
			err := v.Get_files(url, token)
			if err != nil {
				return fmt.Errorf("Error in getting gist %s: %s", k, err)
			}
		}
	}

	return nil
}

func (g GistsCache) Search_gysts(ptrn string) (map[string]string, []string) {
	var files []string

	var gysts map[string]string

	gysts = make(map[string]string, len(g.Gists)-1)

	for k, v := range g.Gists {
		for kk, vv := range v.Files {
			if strings.Contains(vv.Content, ptrn) {
				gysts[kk] = k
				files = append(files, kk)
			}
		}
	}

	return gysts, files
}

/* only need to check the choices field */
func (g GistsCache) IsEmpty() bool {
	return len(g.Choices) == 0
}
