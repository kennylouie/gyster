package request

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Operation struct {
	Name       string
	HttpMethod string
	URL        string
	Payload    []byte
	Headers    map[string]string
}

type Request struct {
	op         *Operation
	response   *http.Response
	body       *bytes.Buffer
	httpClient *http.Client
	Data       []byte
}

func New(op *Operation) *Request {
	return &Request{
		op:         op,
		response:   nil,
		body:       bytes.NewBuffer(op.Payload),
		httpClient: &http.Client{},
		Data:       nil,
	}
}

func (r *Request) Exec() error {
	err := r.send()
	if err != nil {
		return fmt.Errorf("Error in executing request: %s\n", err)
	}

	err = r.httpStatusSuccess()
	if err != nil {
		return fmt.Errorf("Error in status code from response: %s\n", err)
	}

	return nil
}

func (r *Request) send() error {
	req, err := http.NewRequest(
		r.op.HttpMethod,
		r.op.URL,
		bytes.NewBuffer(r.op.Payload),
	)
	if err != nil {
		return fmt.Errorf("Error in constructing request: %s\n", err)
	}

	for k, v := range r.op.Headers {
		req.Header.Add(k, v)
	}

	resp, err := r.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("Error in request: %s\n", err)
	}

	r.response = resp

	r.Data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("Error in decoding response body to bytes: %s\n", err)
	}

	return nil
}

func (r *Request) httpStatusSuccess() error {
	if r.response.StatusCode >= http.StatusBadRequest {
		return fmt.Errorf("Bad status code: %d\n", r.response.StatusCode)
	}

	return nil
}
