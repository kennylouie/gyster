package client

import (
	"fmt"
	"git.cto.ai/sdk-go/pkg/sdk"
	"gyster/internal/libgyster/github"
)

const (
	URL = "https://api.github.com"
)

type Client struct {
	Token      string
	URL        string
	UserName   string
	GistsCache github.GistsCache
}

func New(s *sdk.Sdk) (*Client, error) {
	user, err := s.GetSecret("GYSTER_GITHUB_USER")
	if err != nil {
		return nil, fmt.Errorf("Error in getting GYSTER_GITHUB_USER secret: %s\n", err)
	}

	token, err := s.GetSecret("GYSTER_GITHUB_TOKEN")
	if err != nil {
		return nil, fmt.Errorf("Error in getting GYSTER_GITHUB_TOKEN secret: %s\n", err)
	}

	return &Client{
		Token:      token,
		URL:        URL,
		UserName:   user,
		GistsCache: github.GistsCache{},
	}, nil
}
