package handlers

const (
	SLACK_CHAR_LIMIT = 3000
	NULL_CHAR_SIZE   = 1
	TICKS            = 6
	CHUNK_SIZE       = SLACK_CHAR_LIMIT - NULL_CHAR_SIZE - TICKS
)
