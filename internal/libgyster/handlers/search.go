package handlers

import (
	"fmt"
	"git.cto.ai/sdk-go/pkg/sdk"
	"gyster/internal/libgyster/client"
	"gyster/internal/libgyster/github"
	"gyster/internal/utils"
)

const (
	choose_from_list = "choose from list"
	search_within    = "search within content"
	back             = "back"
)

func Search(s *sdk.Sdk, clnt *client.Client) error {
	if clnt.GistsCache.IsEmpty() {
		err := clnt.GistsCache.Get_gists(clnt.UserName, clnt.URL, clnt.Token)
		if err != nil {
			return fmt.Errorf("Error in getting gists: %s\n", err)
		}
	}

	if clnt.GistsCache.IsEmpty() {
		_ = s.Print("You have no gists, create one!")
		return nil
	}

	sa, err := s.PromptAutocomplete([]string{choose_from_list, search_within, back}, "search_action", "Choose a search action", choose_from_list, "s")
	if err != nil {
		return fmt.Errorf("Could not get search action autocomplete: %s", err)
	}

	var gyst, file string

	switch sa {
	case choose_from_list:
		gyst, file, err = choose_from_list_f(s, clnt)
		if err != nil {
			return fmt.Errorf("Error in choosing from list: %s\n", err)
		}
	case search_within:
		gyst, file, err = search_from_content_f(s, clnt)
		if err != nil {
			return fmt.Errorf("Error in searching from within files: %s\n", err)
		}

		if gyst == "" {
			return nil
		}
	default:
		return nil
	}

	print_gyst(s, clnt.GistsCache.Gists[gyst].Files[file])

	return nil
}

func choose_from_list_f(s *sdk.Sdk, clnt *client.Client) (string, string, error) {
	var gyst, f string

	gyst, err := s.PromptAutocomplete(clnt.GistsCache.Choices, "gyst", "Which gyst would you like to view", clnt.GistsCache.Choices[0], "g")
	if err != nil {
		return gyst, f, fmt.Errorf("Could not get gyst from autocomplete: %s", err)
	}

	if !clnt.GistsCache.Gists[gyst].Has_files() {
		err := clnt.GistsCache.Gists[gyst].Get_files(clnt.URL, clnt.Token)
		if err != nil {
			return gyst, f, fmt.Errorf("Error in getting gyst %s files: %s", f, err)
		}
	}

	files := clnt.GistsCache.Gists[gyst].Get_file_names()

	f, err = s.PromptAutocomplete(files, "file", "Which file would you like to view", files[0], "f")
	if err != nil {
		return gyst, f, fmt.Errorf("Error in getting file from user: %s", err)
	}

	return gyst, f, nil
}

func search_from_content_f(s *sdk.Sdk, clnt *client.Client) (string, string, error) {
	var gyst, f string

	ptrn, err := s.PromptInput("search_str", "Pattern to search", "", "S", false)
	if err != nil {
		return gyst, f, fmt.Errorf("Error in getting search pattern from input: %s", err)
	}

	/* Need to get all the gysts first */
	err = clnt.GistsCache.Get_gists_files(clnt.URL, clnt.Token)
	if err != nil {
		return gyst, f, fmt.Errorf("Error in getting gyst files: %s", err)
	}

	gysts, files := clnt.GistsCache.Search_gysts(ptrn)
	if len(files) == 0 {
		_ = s.Print("No gyst files match that pattern")
		return gyst, f, nil
	}

	f, err = s.PromptAutocomplete(files, "file", "Which file would you like to view", files[0], "f")
	if err != nil {
		return gyst, f, fmt.Errorf("Error in getting file from user: %s", err)
	}

	return gysts[f], f, nil
}

func print_gyst(s *sdk.Sdk, g github.GistFile) {
	border := fmt.Sprintf("=================\n")
	filename := fmt.Sprintf("%-10s: %50v\n", "Filename", g.Filename)
	language := fmt.Sprintf("%-10s: %50v\n", "Language", g.Language)
	size := fmt.Sprintf("%-10s: %50v\n", "Size", g.Size)
	rawurl := fmt.Sprintf("%-10s: %50v\n", "Raw Url", g.RawUrl)
	filetype := fmt.Sprintf("%-10s: %50v\n", "Type", g.Type)

	_ = s.Print(fmt.Sprintf("%s%s%s%s%s%s%s", border, filename, language, size, rawurl, filetype, border))

	printable := utils.SplitBySize(g.Content, CHUNK_SIZE)
	for _, v := range printable {
		_ = s.Print(utils.AppendTicks(v))
	}
}
