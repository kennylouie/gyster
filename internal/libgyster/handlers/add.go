package handlers

import (
	"fmt"
	"git.cto.ai/sdk-go/pkg/sdk"
	"gyster/internal/libgyster/client"
	"gyster/internal/libgyster/github"
	"gyster/internal/utils"
	"strconv"
)

func Add(s *sdk.Sdk, clnt *client.Client) error {
	const (
		view_draft       = "view draft"
		edit_desc        = "edit description"
		edit_filename    = "edit filename"
		edit_public_flag = "edit public"
		edit_content     = "edit content"
		confirm          = "confirm and submit"
		go_back          = "back (discard draft)"
	)

	draft := &github.GistDraft{}

	for {
		sga, err := s.PromptAutocomplete([]string{view_draft, edit_desc, edit_filename, edit_public_flag, edit_content, confirm, go_back}, "start_action", "Choose a action to create a gyst", view_draft, "s")
		if err != nil {
			return fmt.Errorf("Could not get start new gyst action autocomplete: %s\n", err)
		}

		switch sga {
		case view_draft:
			err := print_draft(s, *draft)
			if err != nil {
				return fmt.Errorf("Error in viewing draft: %s\n", err)
			}
		case edit_desc:
			err := edit_description(s, draft)
			if err != nil {
				return fmt.Errorf("Error in editing description: %s\n", err)
			}
		case edit_filename:
			err := edit_filen(s, draft)
			if err != nil {
				return fmt.Errorf("Error in editing filename: %s\n", err)
			}
		case edit_public_flag:
			err := edit_public_f(s, draft)
			if err != nil {
				return fmt.Errorf("Error in editing public flag: %s\n", err)
			}
		case edit_content:
			err := edit_content_f(s, draft)
			if err != nil {
				return fmt.Errorf("Error in editing content: %s\n", err)
			}
		case confirm:
			err := confirm_gyst(s, clnt, *draft)
			if err != nil {
				_ = s.Print(fmt.Sprintf("Error in confirming gyst draft: %s\n", err))
			}
		default:
			return nil
		}
	}
}

func print_draft(s *sdk.Sdk, draft github.GistDraft) error {
	border := fmt.Sprintf("=================\n")
	desc := fmt.Sprintf("%-10s: %50v\n", "Description", draft.Description)
	public := fmt.Sprintf("%-10s: %50v\n", "Public", draft.Public)
	filename := fmt.Sprintf("%-10s: %50v\n", "Filename", draft.Filename)

	err := s.Print(fmt.Sprintf("%s%s%s%s%s", border, desc, public, filename, border))
	if err != nil {
		return fmt.Errorf("Error in printing gyst draft: %s\n", err)
	}

	content := fmt.Sprintf("%-10s:\n", "Content")
	_ = s.Print(content)

	printable := utils.SplitBySize(draft.Content, CHUNK_SIZE)
	for _, v := range printable {
		_ = s.Print(utils.AppendTicks(v))
	}

	return nil
}

func edit_description(s *sdk.Sdk, draft *github.GistDraft) error {
	var current string

	current = draft.Description

	desc, err := s.PromptInput("desc", "Description of gyst", current, "d", false)
	if err != nil {
		return fmt.Errorf("Error in getting description from input: %s\n", err)
	}

	draft.Description = desc

	return nil
}

func edit_public_f(s *sdk.Sdk, draft *github.GistDraft) error {
	var current bool

	current = draft.Public

	public, err := s.PromptList([]string{"true", "false"}, "public", "Public flag of gyst file", strconv.FormatBool(current), "p")
	if err != nil {
		return fmt.Errorf("Error in getting pubic from list: %s\n", err)
	}

	public_bool, err := strconv.ParseBool(public)
	if err != nil {
		return fmt.Errorf("Error in converting string to bool in public response: %s\n", err)
	}

	draft.Public = public_bool

	return nil
}

func edit_filen(s *sdk.Sdk, draft *github.GistDraft) error {
	var current string

	current = draft.Filename

	filename, err := s.PromptInput("filename", "Filename of gyst file", current, "f", false)
	if err != nil {
		return fmt.Errorf("Error in getting filename from input: %s\n", err)
	}

	draft.Filename = filename

	return nil
}

func edit_content_f(s *sdk.Sdk, draft *github.GistDraft) error {
	var current string

	current = draft.Content

	content, err := s.PromptEditor("content", "Edit content", current, "c")
	if err != nil {
		return fmt.Errorf("Error in getting content from editor: %s\n", err)
	}

	draft.Content = content

	return nil
}

func confirm_gyst(s *sdk.Sdk, clnt *client.Client, draft github.GistDraft) error {
	if draft.NotComplete() {
		_ = s.Print("Gist not complete!")
		return nil
	}

	err := print_draft(s, draft)
	if err != nil {
		return fmt.Errorf("Error in viewing draft: %s\n", err)
	}

	confirm, err := s.PromptConfirm("confirm", "Confirm sending draft", "C", false)
	if err != nil {
		return fmt.Errorf("Error in getting confirmation on draft: %s\n", err)
	}

	if confirm {
		err := draft.Create(clnt.URL, clnt.Token)
		if err != nil {
			fmt.Errorf("Error in creating gist: %s\n", err)
		}

		err = clnt.GistsCache.Get_gists(clnt.UserName, clnt.URL, clnt.Token)
		if err != nil {
			return fmt.Errorf("Error in getting gists: %s\n", err)
		}

		_ = s.Print("Gist Created!")
	}

	return nil
}
