package utils

import (
	"fmt"
)

/*
 * Appends coding ticks to a string output
 * Used for code slack outputs
 */
func AppendTicks(input string) string {
	return fmt.Sprintf("```%s```", input)
}

/* Used to parse string characters to the slack sizable output length */
func SplitBySize(input string, size int) []string {
	if len(input) <= size {
		return []string{input}
	}

	var outputs []string

	index := 0

	for i := size; i < len(input); i++ {
		if i == index+size {
			chunk := input[index:i]
			new_line_index := findNewLine(chunk)
			outputs = append(outputs, chunk[0:new_line_index])
			index = index + new_line_index
		}
	}

	outputs = append(outputs, input[index:])

	return outputs
}

/* Finds the last \n char in a string */
func findNewLine(input string) int {
	length := len(input)

	for i := length - 1; i > 0; i-- {
		if input[i] == '\n' {
			return i
		}
	}

	return length
}
