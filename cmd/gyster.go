package main

import (
	"fmt"
	"git.cto.ai/sdk-go/pkg/sdk"
	"gyster/internal/libgyster/client"
	"gyster/internal/libgyster/handlers"
	"os"
)

var (
	search = "search"
	add    = "add"
	exit   = "exit"
)

func main() {
	s := sdk.New()

	clnt, err := client.New(s)
	if err != nil {
		_ = s.Print(fmt.Sprintf("Failed to create client: %s\n", err))
		os.Exit(1)
	}

	for {
		choices := []string{search, add, exit}

		choice, err := s.PromptAutocomplete(choices, "action", "Select an action", "search", "A")
		if err != nil {
			_ = s.Print(fmt.Sprintf("Failed to get action: %s\n", err))
			os.Exit(1)
		}

		switch choice {
		case search:
			err = handlers.Search(s, clnt)
			if err != nil {
				_ = s.Print(fmt.Sprintf("Failed to search: %s\n", err))
				os.Exit(1)
			}
		case add:
			err = handlers.Add(s, clnt)
			if err != nil {
				_ = s.Print(fmt.Sprintf("Failed to create: %s\n", err))
				os.Exit(1)
			}
		default:
			_ = s.Print(fmt.Sprintf("bye\n"))

			return
		}
	}
}
