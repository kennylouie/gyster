############################
# Build container
############################
FROM golang:1.13.4 AS Gbuild

WORKDIR $GOPATH/src/gyster

ADD . .

RUN go build -o gyster ./cmd/gyster.go

############################
# Final container
############################
FROM registry.cto.ai/official_images/base:latest

RUN apt update && apt install -y curl

COPY --from=Gbuild /go/src/gyster/gyster /bin/.
