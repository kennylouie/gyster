# gyster

# Installation

To run, install @cto.ai/ops.

```
ops run @kennylouie/gyster
```

Works in slack as well.

# Build from source

Requires:
+ @cto.ai/sdk-go repo (private for now)
+ @cto.ai/ops

From within the repo:
```
ops build .
```

```
ops run .
```
